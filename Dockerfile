FROM alpine:3 as dl-buildit
ENV BUILD_DEPS="outils-sha512 wget tar coreutils xz"
ENV SYS_DEPS="bash docker-cli"
ENV SHELLCHECK_VERSION="stable"
ADD ./buildit /usr/local/sbin/buildit
ADD ./tests/ /workdir/tests/
ADD ./run_tests /usr/local/sbin/run_tests
RUN { echo -e "\n[INFO] Installing shellcheck build deps." && \
      apk add --no-cache ${BUILD_DEPS} && \
      echo -e "\n[INFO] Retrieving latest stable version of shellcheck." && \
      mkdir -p ~/staging && \
      wget "https://github.com/koalaman/shellcheck/releases/download/stable/shellcheck-stable.linux.x86_64.tar.xz" && \
      tar --xz -xvf shellcheck-"${SHELLCHECK_VERSION}".linux.x86_64.tar.xz && \
      cp shellcheck-"${SHELLCHECK_VERSION}"/shellcheck /usr/bin/ && \
      echo -e "\n[INFO] Testing binary by printing shellcheck version." && \
      shellcheck --version && \
      echo -e "\n[INFO] Purging shellcheck build deps." && \
      rm -rf ~/staging && \
      apk del ${BUILD_DEPS} || exit 1; } && \
    { echo -e "\n[INFO] Displaying distro details:"; \
      cat /etc/*release || exit 1; } && \
    { echo -e "\n[INFO] Making buildit script executable"; \
      chmod u+x /usr/local/sbin/buildit || exit 1; } && \
    { echo -e "\n[INFO] Making run_tests script executable"; \
      chmod u+x /usr/local/sbin/run_tests || exit 1; } && \
    { echo -e "\n[INFO] Updating system package list:"; \
      apk -v update || exit 1; } && \
    { echo -e "\n[INFO] Upgrading installed packages:"; \
      apk -v upgrade || exit 1;} && \
    { for SYS_DEP in ${SYS_DEPS}; do \
        echo -e "\n[INFO] Installing sys dep: ${SYS_DEP}"; \
        apk -v add --no-cache "${SYS_DEP}"; \
      done || exit 1; } && \
    { echo -e "\n[INFO] Displaying sys packages stats and installed list:"; \
      { apk -v stats; apk -v list --installed; } || exit 1; };
WORKDIR /workdir
CMD /bin/bash
