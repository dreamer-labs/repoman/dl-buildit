# [2.1.0](https://gitlab.com/dreamer-labs/repoman/dl-buildit/compare/v2.0.0...v2.1.0) (2021-10-20)


### Bug Fixes

* Ensure non-null for `IMAGE` ([c400554](https://gitlab.com/dreamer-labs/repoman/dl-buildit/commit/c400554))


### Features

* Add FINAL_TARGET_FLAG option ([9f7448f](https://gitlab.com/dreamer-labs/repoman/dl-buildit/commit/9f7448f))

# [2.0.0](https://gitlab.com/dreamer-labs/repoman/dl-buildit/compare/v1.4.0...v2.0.0) (2019-12-18)


### Features

* change default image name ([5d40c5d](https://gitlab.com/dreamer-labs/repoman/dl-buildit/commit/5d40c5d))


### BREAKING CHANGES

* This commit changes the default image created
when no `IMAGE` variable is defined. By default the image will
not have an appended name now. Previously all images were appended
with `/image'.

# [1.4.0](https://gitlab.com/dreamer-labs/repoman/dl-buildit/compare/v1.3.0...v1.4.0) (2019-10-31)


### Features

* Add support for passing --build-args ([bdc761e](https://gitlab.com/dreamer-labs/repoman/dl-buildit/commit/bdc761e))

# [1.3.0](https://gitlab.com/dreamer-labs/repoman/dl-buildit/compare/v1.2.0...v1.3.0) (2019-10-29)


### Features

* Add more testing and input validation ([10b6ae5](https://gitlab.com/dreamer-labs/repoman/dl-buildit/commit/10b6ae5))

# [1.2.0](https://gitlab.com/dreamer-labs/repoman/dl-buildit/compare/v1.1.0...v1.2.0) (2019-10-23)


### Features

* Add USE_CACHE option ([4f52093](https://gitlab.com/dreamer-labs/repoman/dl-buildit/commit/4f52093))

# [1.1.0](https://gitlab.com/dreamer-labs/repoman/dl-buildit/compare/v1.0.1...v1.1.0) (2019-10-22)


### Features

* Add BuildKit support ([d15b6c2](https://gitlab.com/dreamer-labs/repoman/dl-buildit/commit/d15b6c2))

## [1.0.1](https://gitlab.com/dreamer-labs/repoman/dl-buildit/compare/v1.0.0...v1.0.1) (2019-10-21)


### Bug Fixes

* Added additional debugging messaging to buildit ([0151d24](https://gitlab.com/dreamer-labs/repoman/dl-buildit/commit/0151d24))

# 1.0.0 (2019-10-21)


### Features

* initial commit ([75a22af](https://gitlab.com/dreamer-labs/repoman/dl-buildit/commit/75a22af))
